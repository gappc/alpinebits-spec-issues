# Inventory HotelInfo push (OTA_HotelDescriptiveContentNotifRQ)

> GuestRooms may only have a Code attribute and may contain MultimediaDescription sub-elements.

_AlpineBits_2017-10.pdf, page 42_

## Investigation

After further investigation, the RNG schema seems to be correct: there are 3 different definitions of GuestRoom for different purposes:

- basic content + basic descriptions + optionally additional description
- additional detail only
- room list

The RNG validator uses the best match for a given input XML. While writing Unit Tests, there was the desire to test the negative outcome for "additional detail only" with a malformed XML, where the FacilityInfo->GuestRooms->GuestRoom element is only allowed to contain a Code attribute and a MultimediaDescriptions element. The test defines a MinOccupancy element, which is not allowed for that purpose (see XML below).

```
<?xml version="1.0"?>
<OTA_HotelDescriptiveContentNotifRQ
    xmlns="http://www.opentravel.org/OTA/2003/05"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        Version="5.000"TimeStamp="2014-10-16T17:00:40" 
        xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05
        OTA_HotelDescriptiveContentNotifRQ.xsd">
    <HotelDescriptiveContents>
        <HotelDescriptiveContent HotelCode="9996">
            <FacilityInfo>
                <GuestRooms>
                    <!-- Err: MinOccupancy is forbidden -->
                    <GuestRoom Code="DZ" MinOccupancy="1"></GuestRoom>
                </GuestRooms>
            </FacilityInfo>
        </HotelDescriptiveContent>
    </HotelDescriptiveContents>
</OTA_HotelDescriptiveContentNotifRQ>
```

In this case, the RNG validator uses the definition for the purpose "basic content + basic descriptions + optionally additional description", which is correct, but not what was wanted. Unfortunately there seems to be no possibility to further instruct the used RNG validator (at least in Java), so it is not possible to write unit tests for this case.

In summary: the RNG definition of Inventory/HotelInfo push is correct. The negative outcome of the forbidden MinOccupancy element can not be tested by RNG.

The GitLab issue [https://gitlab.com/alpinebits/hoteldata/standard-specification/issues/39](https://gitlab.com/alpinebits/hoteldata/standard-specification/issues/39) can be closed, as there is nothing wrong.