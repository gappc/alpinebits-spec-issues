# Inventory/HotelInfo push (OTA_HotelDescriptiveContentNotifRQ)

## Problem description

The written 2017-10 and 2018-10 standard allows the values `PlainText` and `HTML` for the `TextFormat` attribute of a OTA_HotelDescriptiveContentNotifRQ document.

> The attribute TextFormat is set to PlainText or HTML and the attribute Language is set to a two-letter lowercase language abbreviation according to ISO 639-1

_AlpineBits_2017-10.pdf, page 38_ and _AlpineBits_2018-10.pdf, page 42_

> Zero or more Description elements follow, under the same rules outlined above for descriptions contained in the element TextItem.

_AlpineBits_2018-10.pdf, page 43_ and _AlpineBits_2020-10.pdf, page 46_

Here are the problems:

- the 2017-10 XSD and RNG definitions allow only the value `PlainText`, which contradicts the standard.
- the 2018-10 and 2020-10 XSD definition allows only the value `PlainText` for the `TextFormat` of an `ImageItem->Description` element, which contradicts the standard.

Original XSD:

- `alpinebits-2017-10.xsd`: line 1412 - 1418, line 1460 - 1466
- `alpinebits-2018-10.xsd`: line 1740 - 1748
- `alpinebits-2018-10.xsd`: line 2344 - 2350

```
<xs:attribute name="TextFormat" use="required">
    <xs:simpleType>
        <xs:restriction base="xs:string">
            <xs:enumeration value="PlainText"/>
        </xs:restriction>
    </xs:simpleType>
</xs:attribute>
```

Original RNG:

- `alpinebits-2017-10.rng`: line 1488 - 1492, line 1537 - 1541, line 1592 - 1596, line 1671 - 1675

```
<rng:attribute name="TextFormat">
    <rng:choice>
        <rng:value>PlainText</rng:value>
    </rng:choice>
</rng:attribute>
```

## Proposed solution

Fixed XSD:

- `alpinebits-2017-10.xsd.fixed`
- `alpinebits-2018-10.xsd.fixed`
- _same fix as in 2018-10_

```
<xs:attribute name="TextFormat" use="required">
    <xs:simpleType>
        <xs:restriction base="xs:string">
            <xs:enumeration value="PlainText"/>
            <xs:enumeration value="HTML"/>
        </xs:restriction>
    </xs:simpleType>
</xs:attribute>
```

XSD patches:

- `alpinebits-2017-10.xsd.patch`
- `alpinebits-2018-10.xsd.patch`

Fixed RNG:

- `alpinebits-2017-10.rng.fixed`

```
<rng:attribute name="TextFormat">
    <rng:choice>
        <rng:value>PlainText</rng:value>
        <rng:value>HTML</rng:value>
    </rng:choice>
</rng:attribute>
```

RNG patches:

- `alpinebits-2017-10.rng.patch`
