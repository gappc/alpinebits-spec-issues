# Inventory HotelInfo push (OTA_HotelDescriptiveContentNotifRQ)

> AlpineBits ® defines some values for the ID attribute that should be recognized by servers and clients:
>
> - WEBSITE: for the lodging structure’s official website
> - TRUSTYOU: for the lodging structure’s presence on trustyou.com
> - TRIPADVISOR: for the lodging structure’s presence on tripadvisor.com
> - TWITTER: for the lodging structure’s presence on twitter.com
> - FACEBOOK: for the lodging structure’s presence on facebook.com
> - INSTAGRAM: for the lodging structure’s presence on instagram.com
> - YOUTUBE: for the lodging structure’s presence on youtube.com
>
> Partners are allowed to define further values but the value of the attribute must be all uppercase.

_AlpineBits_2020-10.pdf, page 55_

## Investigation

The standard says, that "Partners are allowed to define further values...". Unfortunately, the XSD and RNG files restrict the ID values to be exactly one of the values above:

```XML
<!-- Extract from AlpineBits 2020-10 XSD file -->
...
<xs:element name="URLs" minOccurs="0">
    <xs:complexType>
    <xs:sequence>
        <xs:element name="URL"  maxOccurs="unbounded">
        <xs:complexType>
            <xs:simpleContent>
                <xs:extension base="xs:anyURI">
                <xs:attribute name="ID">
                    <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:enumeration value="WEBSITE"/>
                        <xs:enumeration value="TRUSTYOU"/>
                        <xs:enumeration value="TRIPADVISOR"/>
                        <xs:enumeration value="TWITTER"/>
                        <xs:enumeration value="FACEBOOK"/>
                        <xs:enumeration value="INSTAGRAM"/>
                        <xs:enumeration value="YOUTUBE"/>
                    </xs:restriction>
                    </xs:simpleType>
                </xs:attribute>
                </xs:extension>
            </xs:simpleContent>
            </xs:complexType>
        </xs:element>
    </xs:sequence>
    </xs:complexType>
</xs:element>
...
```

```XML
<!-- Extract from AlpineBits 2020-10 RNG file -->
...
<rng:element name="URL">
    <rng:ref name="def_nonempty_string"/>
    <rng:optional>
        <rng:attribute name="ID">
        <rng:choice>
            <rng:value>WEBSITE</rng:value>
            <rng:value>TRUSTYOU</rng:value>
            <rng:value>TRIPADVISOR</rng:value>
            <rng:value>TWITTER</rng:value>
            <rng:value>FACEBOOK</rng:value>
            <rng:value>INSTAGRAM</rng:value>
            <rng:value>YOUTUBE</rng:value>
        </rng:choice>
        </rng:attribute>
    </rng:optional>
</rng:element>
...
```

In summary: the XSD and RNG definitions of the Inventory/HotelInfo URL elements contradict the written standard.
