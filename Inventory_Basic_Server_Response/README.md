# Inventory/Basic (pull) server response (OTA_HotelDescriptiveInfoRS)

## Problem description

The spezification states, that the OTA_HotelDescriptiveInfoRS returns one of the four possible AlpineBits server response outcomes (success,​ advisory,​ warning​ or error).

> The server will send a response indicating the outcome of the request. The response is a
> OTA_HotelDescriptiveInfoRS document. Any of the four possible AlpineBits ® server response
> outcomes (success, advisory, warning or error) are allowed.

_AlpineBits_2017-10.pdf, page 41_, _AlpineBits_2018-10.pdf, page 45_ and _AlpineBits_2020-10.pdf, page 47_

This is not how the OTA_HotelDescriptiveInfoRS element is defined in XSD and RNG schemas. The schemas know only about success and error.

Original XSD:

- `alpinebits-2017-10.xsd`: line 1116 - 1159
- `alpinebits-2018-10.xsd`: line 761 - 806
- `alpinebits-2020-10.xsd`: line 1074 - 1119

Original RNG:

- `alpinebits-2017-10.rng`: line 1155 - 1204
- `alpinebits-2018-10.rng`: line 895 - 954
- `alpinebits-2017-10.rng`: line 1213 - 1272

## Proposed solution

At the moment, no solution is proposed. A fix and patch needs to be discussed.
