# RNG schema general

## Problem description

The 2018-10 RNG file defines the attribute `Code` twice on line 489 to 496 with some wrong nesting, possibly as result of an unintentional duplication. This makes the RNG file invalid (tested e.g. with [https://www.liquid-technologies.com/online-relaxng-validator](https://www.liquid-technologies.com/online-relaxng-validator)).

Original RNG:

- `alpinebits-2018-10.rng`: line 488 - 502

```
<rng:optional>
<rng:optional>
    <rng:attribute name="Code">
        <rng:ref name="def_int_ge0"/>
    </rng:attribute>
</rng:optional>
    <rng:attribute name="Code">
        <rng:ref name="def_int_ge0"/>
    </rng:attribute>
</rng:optional>
<rng:optional>
    <rng:attribute name="Status">
        <rng:ref name="def_send_complete"/>
    </rng:attribute>
</rng:optional>
```

## Proposed solution

Fixed RNG:

- `alpinebits-2018-10.rng.fixed`

```
<rng:optional>
    <rng:attribute name="Code">
        <rng:ref name="def_int_ge0"/>
    </rng:attribute>
</rng:optional>
<rng:optional>
    <rng:attribute name="Status">
        <rng:ref name="def_send_complete"/>
    </rng:attribute>
</rng:optional>
```

RNG patches:

- `alpinebits-2018-10.rng.patch`